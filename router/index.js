const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");

router.get('/index',(req,res)=>{
    const params ={
        numero:req.query.numero,
        destino:req.query.destino,
        nombre:req.query.nombre,
        edad:req.query.edad,
        tipoViaje:req.query.tipoViaje,
        precio:req.query.precio
        
    }
    res.render('index.html',params);

})

router.post('/index',(req,res)=>{
    const params ={
        numero:req.body.numero,
        destino:req.body.destino,
        nombre:req.body.nombre,
        edad:req.body.edad,
        tipoViaje:req.body.tipoViaje,
        precio:req.body.precio
    }
    res.render('index.html',params);
})

module.exports=router;

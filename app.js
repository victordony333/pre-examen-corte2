const http= require("http");
const express = require("express");
const bodyparser = require("body-parser");

const misRutas = require("./router/index.js");
const path = require("path");

async: true

const app =express();
app.set("view engine", "ejs");
app.use(express.static(__dirname + '/public'));
app.use(bodyparser.urlencoded({encoded:true}));

//cambiar extenciones ejs a html 
app.engine("html",require('ejs').renderFile);
app.use(misRutas);

app.use((req,res,next)=>{
    res.status(404).sendFile(__dirname  + '/public/error.html')
})

const puerto =3000;
app.listen(puerto,()=>{

    console.log("Iniciando puerto");
});